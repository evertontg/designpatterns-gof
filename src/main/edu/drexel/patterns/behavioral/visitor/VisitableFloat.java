package main.edu.drexel.patterns.behavioral.visitor;

/** Concrete Element */
public class VisitableFloat implements Visitable{

    private Float value;

    // Constructor
    public VisitableFloat(float f) {
        value = new Float( f );
    }

    public Float getFloat() {
        return value;
    }

    @Override
    public void accept( Visitor visitor ) {
        visitor.visit( this );
    }

}
