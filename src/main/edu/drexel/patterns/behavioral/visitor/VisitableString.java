package main.edu.drexel.patterns.behavioral.visitor;

/** Concrete Element */
public class VisitableString implements Visitable{

    private String value;

    // Constructor
    public VisitableString(String string) {
        value = string;
    }

    public String getString() {
        return value;
    }

    @Override
    public void accept( Visitor visitor ) {
        visitor.visit( this );
    }

}
