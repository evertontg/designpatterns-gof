package main.edu.drexel.patterns.behavioral.visitor;

/** Element (interface) */
public interface Visitable {
    void accept( Visitor visitor ) ;
}
