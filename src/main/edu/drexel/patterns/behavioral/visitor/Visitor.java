package main.edu.drexel.patterns.behavioral.visitor;

import java.util.Collection;

/** Visitor (interface) */
public interface Visitor {

    void visit( Collection collection );
    void visit( VisitableString string );
    void visit( VisitableFloat nFloat );

}
