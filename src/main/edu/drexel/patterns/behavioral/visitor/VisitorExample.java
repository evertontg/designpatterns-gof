package main.edu.drexel.patterns.behavioral.visitor;

import java.util.ArrayList;
import java.util.List;

/** Client */
public class VisitorExample {

    public static void main (String[] arg) {
        // Prepare a heterogeneous collection

        List listA = new ArrayList();
        listA.add( new VisitableString( "A string" ) );
        listA.add( new VisitableFloat( 1 ) );

        List listB = new ArrayList();
        listB.add( new VisitableString( "Another string" ) );
        listB.add( new VisitableFloat( 2 ) );

        listA.add( listB);
        listA.add( new VisitableFloat( 3 ) );
        listA.add( new Double( 4 ) );

        // Visit the collection
        Visitor visitor = new WatcherVisitor();
        visitor.visit( listA );
    }
}
