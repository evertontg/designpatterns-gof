package main.edu.drexel.patterns.behavioral.strategy;

/** Context */
public class MyArray {

    private int[] array;
    private int size;

    ArrayDisplayFormat format;

    public MyArray( int size ) {
        array = new int[ size ];
    }

    public int[] getArray() {
        return array;
    }

    public void setArray(int[] array) {
        this.array = array;
    }


    public int getSize() {
        return size;
    }

    public ArrayDisplayFormat getFormat() {
        return format;
    }

    public void setFormat(ArrayDisplayFormat format) {
        this.format = format;
    }

    public void setValue( int pos, int value ) {
        array[pos] = value;
    }

    public void display() {
        format.printFormatedData( array );
    }

}
