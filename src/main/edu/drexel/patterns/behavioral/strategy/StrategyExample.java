package main.edu.drexel.patterns.behavioral.strategy;

public class StrategyExample {

    public static void main (String[] arg) {

        MyArray m = new MyArray( 10 );
        m.setValue( 1 , 6 );
        m.setValue( 0 , 8 );
        m.setValue( 4 , 1 );
        m.setValue( 9 , 7 );

        // Test A
        System.out.println("This is the array in ’standard’ format");
        m.setFormat( new StandardFormat() );
        m.display();

        // Test B
        System.out.println("This is the array in ’math’ format:");
        m.setFormat( new MathFormat() );
        m.display();
    }
}
