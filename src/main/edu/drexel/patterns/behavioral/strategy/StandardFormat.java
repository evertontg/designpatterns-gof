package main.edu.drexel.patterns.behavioral.strategy;

/** Concrete Strategy */
public class StandardFormat implements ArrayDisplayFormat{

    @Override
    public void printFormatedData(int[] arr ) {
        System.out.print( "{ " );
        for(int i=0; i < arr.length-1 ; i++ )
            System.out.print( arr[i] + ", " );
        System.out.println( arr[arr.length-1] + " }" );
    }
}
