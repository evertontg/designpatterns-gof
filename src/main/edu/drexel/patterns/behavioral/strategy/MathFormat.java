package main.edu.drexel.patterns.behavioral.strategy;

/** Concrete Strategy */
public class MathFormat implements ArrayDisplayFormat {

    @Override
    public void printFormatedData(int[] arr ) {
        for(int i=0; i < arr.length ; i++ )
            System.out.println( "Arr[ " + i + " ] = " + arr[i] );
    }
}
