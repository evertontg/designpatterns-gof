package main.edu.drexel.patterns.behavioral.strategy;

/** Strategy */

public interface ArrayDisplayFormat {
    public void printFormatedData(int[] arr );
}
