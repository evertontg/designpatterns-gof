package main.edu.drexel.patterns.behavioral.observer;

public class ObserverExample {

    public static void main (String[] args) {

        // Subject
        ObservedSubject subject = new ObservedSubject() ;

        // Observer
        Watcher watcher = new Watcher();

        // Observer
        Psychologist psychologist = new Psychologist();

        // Adding new observers to the subject
        subject.addObserver( watcher );
        subject.addObserver( psychologist );

        for(int i=1;i<=10;i++){
            System.out.println( "Main : Do you like the number " + i +"?" );
            // when subjects state change, the observers will be notified.
            subject.receiveValue( i );
        }

        System.out.println( "The Subject has changed "
                + watcher.observedChanges()
                + " times the internal value.");

        System.out.println("The Psychologist opinion is:" + psychologist.opinion() );
    }
}
