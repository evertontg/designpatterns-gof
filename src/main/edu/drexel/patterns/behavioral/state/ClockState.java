package main.edu.drexel.patterns.behavioral.state;

/** State */
public abstract class ClockState {

    protected Clock clock ;

    // Constructor
    public ClockState(Clock clock) {
        this.clock = clock;
    }

    public abstract void modeButton();
    public abstract void changeButton();
}
