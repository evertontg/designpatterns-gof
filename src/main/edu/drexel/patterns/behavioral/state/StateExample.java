package main.edu.drexel.patterns.behavioral.state;

public class StateExample {

    public static void main ( String arg[] ) {

        Clock theClock = new Clock();
        theClock.changeButton();
        theClock.modeButton();

        theClock.changeButton();
        theClock.changeButton();

        theClock.modeButton();
        theClock.changeButton();
        theClock.changeButton();
        theClock.changeButton();

        theClock.changeButton();
        theClock.modeButton();
    }
}
