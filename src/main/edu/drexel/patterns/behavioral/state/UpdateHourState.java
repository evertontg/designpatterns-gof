package main.edu.drexel.patterns.behavioral.state;

/** Concrete State */
public class UpdateHourState extends ClockState{

    public UpdateHourState(Clock clock) {
        super( clock );
        System.out.println(
                "** UPDATING HR: Press CHANGE button to increase hours.");
    }

    @Override
    public void modeButton() {
        clock.setState( new UpdateMinutesState( clock ) );
    }

    @Override
    public void changeButton() {
        clock.hr++;
        if(clock.hr == 24)
            clock.hr = 0;
        System.out.print( "CHANGE pressed - ");
        clock.showTime();
    }
}
