package main.edu.drexel.patterns.behavioral.state;

/** Concrete State */
public class NormalDisplayState extends ClockState {

    public NormalDisplayState(Clock clock) {
        super( clock );
        System.out.println( "** Clock is in normal display.");
    }

    @Override
    public void modeButton() {
        clock.setState( new UpdateHourState( clock ) );
    }

    @Override
    public void changeButton() {
        System.out.print( "LIGHT ON: " );
        clock.showTime();
    }
}
