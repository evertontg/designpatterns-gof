package main.edu.drexel.patterns.creational.builder;

import java.util.Enumeration;
import java.util.Hashtable;

/** Product */
public class ERModel {

    private Hashtable modelEntities = new Hashtable();
    private Hashtable modelRelations = new Hashtable();

    public void addEntity( String name ) {
        modelEntities.put( name, new Entity( name ) );
    }

    public void addRelationship(String entity1, String entity2, String relation ) {

        Relationship rel = new Relationship();

        rel.setName(relation);
        rel.setEntity1((Entity) modelEntities.get(entity1));
        rel.setEntity2((Entity) modelEntities.get(entity2));

        modelRelations.put( relation, rel );
    }

    public void addCardMin( String entity, String relation, String value ) {
        Relationship rel = (Relationship)
                modelRelations.get( relation );
        if( entity.equals( rel.getEntity1().getName() ) )
            rel.setCardMin1(value);
        else
            rel.setCardMin2(value);
    }

    public void addCardMax( String entity, String relation, String value ) {
        Relationship rel = (Relationship)
                modelRelations.get( relation );
        if( entity.equals( rel.getEntity1().getName() ) )
            rel.setCardMax1(value);
        else
            rel.setCardMax2(value);
    }

    public void showStructure( ) {
        for(Enumeration elem = modelRelations.elements();
            elem.hasMoreElements() ; ) {
            Relationship currRel = (Relationship) elem.nextElement() ;
            System.out.println( "[ " + currRel.getEntity1().getName() +
                    " ]--("+ currRel.getCardMin1() + "," +
                    currRel.getCardMax1()
                    + ")------< " + currRel.getName() +
                    " >------("+ currRel.getCardMin2() + ","
                    + currRel.getCardMax2() + ")--[ " +
                    currRel.getEntity2().getName() + " ]" );
        }
    }
}


