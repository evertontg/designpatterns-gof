package main.edu.drexel.patterns.creational.builder;

/** Model */
public class Relationship {

    private String name;
    private Entity entity1, entity2;
    private String cardMin1, cardMax1, cardMin2, cardMax2 ;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Entity getEntity1() {
        return entity1;
    }

    public void setEntity1(Entity entity1) {
        this.entity1 = entity1;
    }

    public Entity getEntity2() {
        return entity2;
    }

    public void setEntity2(Entity entity2) {
        this.entity2 = entity2;
    }

    public String getCardMin1() {
        return cardMin1;
    }

    public void setCardMin1(String cardMin1) {
        this.cardMin1 = cardMin1;
    }

    public String getCardMax1() {
        return cardMax1;
    }

    public void setCardMax1(String cardMax1) {
        this.cardMax1 = cardMax1;
    }

    public String getCardMin2() {
        return cardMin2;
    }

    public void setCardMin2(String cardMin2) {
        this.cardMin2 = cardMin2;
    }

    public String getCardMax2() {
        return cardMax2;
    }

    public void setCardMax2(String cardMax2) {
        this.cardMax2 = cardMax2;
    }
}
