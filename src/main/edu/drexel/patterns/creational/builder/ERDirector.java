package main.edu.drexel.patterns.creational.builder;

/** Director */
public class ERDirector {

    public static Object getModel( ModelBuilder builder ) {

        builder.addEntity( "Student" );
        builder.addEntity( "University" );
        builder.addEntity( "Professor" );

        builder.addRelationship( "Student", "University","Studies at" );

        builder.addCardMin( "Student", "Studies at", "1" );
        builder.addCardMax( "Student", "Studies at", "1" );
        builder.addCardMin( "University", "Studies at", "0" );
        builder.addCardMax( "University", "Studies at", "N" );

        builder.addRelationship( "University", "Assistant Professor","Has" );

        builder.addCardMin( "University", "Has", "0" );
        builder.addCardMax( "University", "Has", "N" );
        builder.addCardMin( "AssistantProfessor", "Has", "1" );
        builder.addCardMax( "AssistantProfessor", "Has", "N" );

        builder.addRelationship( "University", "Professor","Has" );

        builder.addCardMin( "University", "Has", "0" );
        builder.addCardMax( "University", "Has", "N" );
        builder.addCardMin( "FullProfessor", "Has", "1" );
        builder.addCardMax( "FullProfessor", "Has", "N" );

        return builder.getModel();
    }
}
