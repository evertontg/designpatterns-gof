package main.edu.drexel.patterns.creational.builder;

/** Model */
public class Entity {

    public String name;

    public Entity( String name ) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
