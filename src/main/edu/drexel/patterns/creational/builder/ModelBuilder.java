package main.edu.drexel.patterns.creational.builder;

/** (Abstract) Builder */
public abstract class ModelBuilder {

    void addEntity( String name ) {};
    void addRelationship(String fromEntity, String toEntity, String relation ) {};
    void addCardMin(String entity, String relation, String value ) {};
    void addCardMax( String entity, String relation, String value ) {};
    abstract Object getModel();
}
