package main.edu.drexel.patterns.creational.factorymethod;

/** Concrete Creator */
public class ConnectorHandler extends ElementHandler{

    public MapElement getNewElement() {
        return new Connector();
    }

    public void connect(Connector connector, Place origin, Place destination) {
        connector.setConnection( origin, destination );
    }
}
