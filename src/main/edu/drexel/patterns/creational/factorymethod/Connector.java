package main.edu.drexel.patterns.creational.factorymethod;

/** Concrete Product */
public class Connector implements MapElement{

    private String connectorLabel;

    Place place1, place2;

    public void setConnection( Place origin, Place destination ) {
        place1 = origin;
        place2 = destination;
    }

    @Override
    public void setLabel( String label ) {
        connectorLabel = label;
    }

    @Override
    public String getData() {
        return connectorLabel + " [from " +
                place1.getData() + " to " +
                place2.getData() + "]";
    }
}
