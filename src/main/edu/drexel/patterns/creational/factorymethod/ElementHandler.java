package main.edu.drexel.patterns.creational.factorymethod;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/** Creator */
public abstract class ElementHandler {

    public MapElement createElement( ) throws IOException {
        BufferedReader reader = new BufferedReader( new InputStreamReader(System.in) );

        System.out.println( "Enter a label for the element: ");

        String label = reader.readLine();
        MapElement element = getNewElement( );
        element.setLabel( label );

        return element;
    }

    public abstract MapElement getNewElement();

    public void paintElement(MapElement element) {
        System.out.println( element.getData() );
    }
}
