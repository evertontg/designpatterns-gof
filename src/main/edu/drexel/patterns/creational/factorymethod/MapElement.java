package main.edu.drexel.patterns.creational.factorymethod;

/** Product */
public interface MapElement {

    void setLabel( String id );
    String getData();
}
