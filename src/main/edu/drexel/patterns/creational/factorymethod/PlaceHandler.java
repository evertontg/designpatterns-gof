package main.edu.drexel.patterns.creational.factorymethod;

/** Concrete Creator */
public class PlaceHandler extends ElementHandler{

    public MapElement getNewElement() {
        return new Place();
    }
}
