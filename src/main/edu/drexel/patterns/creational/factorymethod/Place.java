package main.edu.drexel.patterns.creational.factorymethod;

/** Concrete Product */
public class Place implements MapElement{

    private String placeLabel;

    @Override
    public void setLabel( String label ) {
        placeLabel = label;
    }

    @Override
    public String getData() {
        return "city: "+ placeLabel;
    }
}
