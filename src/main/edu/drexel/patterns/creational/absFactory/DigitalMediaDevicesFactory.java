package main.edu.drexel.patterns.creational.absFactory;

/** Concrete Factory */
public class DigitalMediaDevicesFactory implements DevicesFactory {

    @Override
    public Player createPlayList() {
        return new DigitalMediaPlayer();
    }

    @Override
    public Recorder createRecorder() {
        return new DigitalMediaRecorder();
    }

    @Override
    public Media createMedia() {
        return new DigitalMedia();
    }
}
