package main.edu.drexel.patterns.creational.absFactory;

/** Abstract Factory **/
public interface DevicesFactory {

    Player createPlayList();
    Recorder createRecorder();
    Media createMedia();
}
