package main.edu.drexel.patterns.creational.absFactory;

/** Abstract Product **/
public interface Player {

    void accept (Media media);
    void play();
}
