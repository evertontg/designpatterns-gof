package main.edu.drexel.patterns.creational.absFactory;

/** Concrete Factory */
public class BluerayDevicesFactory implements DevicesFactory {

    @Override
    public Player createPlayList() {
        return new BluerayPlayer();
    }

    @Override
    public Recorder createRecorder() {
        return new BluerayRecorder();
    }

    @Override
    public Media createMedia() {
        return new Blueray();
    }
}
