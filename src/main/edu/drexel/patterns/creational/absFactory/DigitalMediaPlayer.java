package main.edu.drexel.patterns.creational.absFactory;

/** Concrete Product **/
public class DigitalMediaPlayer implements Player {

    DigitalMedia file;

    @Override
    public void accept(Media media) {
        file = (DigitalMedia) media;
    }

    @Override
    public void play() {
        if(file == null){
            System.out.println("Error. Please chose a media");
        }else{
            System.out.println( file.play() );
        }
    }
}
