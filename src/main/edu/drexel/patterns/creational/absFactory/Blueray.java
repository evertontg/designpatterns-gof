package main.edu.drexel.patterns.creational.absFactory;

/** Concrete Product **/
public class Blueray implements Media {

    private String track = "";

    public void writeOnDisk(String sound){
        track = sound;
    }

    public String readDisk(){
        return track;
    }

}
