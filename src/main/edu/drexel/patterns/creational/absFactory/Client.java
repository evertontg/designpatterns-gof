package main.edu.drexel.patterns.creational.absFactory;

/** Client */
public class Client {

    DevicesFactory devicesFactory;

    public void selectFactory(DevicesFactory df){
        devicesFactory = df;
    }

    public void test(String song){

        Media media = devicesFactory.createMedia();
        Recorder recorder = devicesFactory.createRecorder();
        Player player = devicesFactory.createPlayList();

        recorder.accept(media);
        System.out.println( "Recording the song : " + song );
        recorder.record( song );

        System.out.println( "Listening the record:" );
        player.accept( media );

        player.play();
    }
}
