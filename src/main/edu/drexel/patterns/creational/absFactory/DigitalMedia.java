package main.edu.drexel.patterns.creational.absFactory;

/** Concrete Product **/
public class DigitalMedia implements Media {

    private String tape = "";

    public void saveOnTape(String sound){
        tape = sound;
    }

    public String play(){
        return tape;
    }
}
