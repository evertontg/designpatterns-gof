package main.edu.drexel.patterns.creational.absFactory;

/** Concrete Product **/
public class BluerayRecorder implements Recorder {

    Blueray cdInside;

    @Override
    public void accept(Media media) {
        cdInside = (Blueray) media;
    }

    @Override
    public void record(String sound) {
        if(cdInside == null){
            System.out.println("Error. No Blueray.");
        }else{
            cdInside.writeOnDisk(sound);
        }
    }
}
