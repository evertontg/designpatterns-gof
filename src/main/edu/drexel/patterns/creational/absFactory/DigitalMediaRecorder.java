package main.edu.drexel.patterns.creational.absFactory;

/** Concrete Product **/
public class DigitalMediaRecorder implements Recorder {

    DigitalMedia tapeInside;

    @Override
    public void accept(Media media) {
        tapeInside = (DigitalMedia) media;
    }

    @Override
    public void record(String sound) {
        if(tapeInside == null){
            System.out.println("Error. Insert a DigitalMedia");
        }else{
            tapeInside.saveOnTape(sound);
        }
    }
}
