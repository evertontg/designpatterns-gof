package main.edu.drexel.patterns.creational.absFactory;

/** Concrete Product **/
public class BluerayPlayer implements Player{

    Blueray blueray;

    @Override
    public void accept(Media media) {
        blueray = (Blueray) media;
    }

    @Override
    public void play() {
        if(blueray == null){
            System.out.println("Error. No Blueray");
        }else{
            System.out.println( blueray.readDisk() );
        }
    }
}
