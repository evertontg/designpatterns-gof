package main.edu.drexel.patterns.creational.absFactory;

/** Abstract Product **/
public interface Recorder {

    void accept (Media media);
    void record (String sound);
}
