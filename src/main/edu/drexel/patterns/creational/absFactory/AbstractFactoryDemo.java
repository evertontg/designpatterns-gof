package main.edu.drexel.patterns.creational.absFactory;

public class AbstractFactoryDemo {

    public static void main(String[] args) {

        Client client = new Client();

        System.out.println( " Testing tape devices " );
        client.selectFactory( new DigitalMediaDevicesFactory() );
        client.test( "I wanna hold your hand..." );

        System.out.println( " Testing Blueray devices " );
        client.selectFactory( new BluerayDevicesFactory() );
        client.test( "Fly me to the moon..." );
    }
}
