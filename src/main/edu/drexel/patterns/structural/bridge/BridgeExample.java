package main.edu.drexel.patterns.structural.bridge;

import java.util.ArrayList;

public class BridgeExample {

    public static final int ROWS = 3;
    public static final int COLS = 4;

    public static void main( String[] arg ) {

        Matrix matrixCA = new CompleteMatrix( ROWS, COLS, new SArrayList() );

        System.out.println( "Complete Matrix with ArrayList:");

        try {
            matrixCA.put( 1, 2, 1 );
            matrixCA.put( 2, 1, 2 );
            matrixCA.put( 0, 3, 3 );
            matrixCA.put( 1, 2, 0 );
        } catch (MatrixIndexOutOfBoundsException e) {
            e.printStackTrace();
        }

        for(int i = 0 ; i< ROWS; i++ ) {
            for(int j = 0 ; j< COLS; j++ )
                try {
                    System.out.print( matrixCA.get( i, j )+" " );
                } catch (MatrixIndexOutOfBoundsException e) {
                    System.out.printf("Exception captured: %s %n", e.getMessage().toString());
                }
            System.out.println();
        }

        Matrix matrixSA = new SparseMatrix( ROWS, COLS, new SArrayList() );
        System.out.println( "Sparse Matrix with ArrayList:");

        try {
            matrixSA.put( 1, 2, 1 );
            matrixSA.put( 2, 1, 2 );
            matrixSA.put( 0, 3, 3 );
            matrixSA.put( 1, 2, 0 );
        } catch (MatrixIndexOutOfBoundsException e) {
            System.out.printf("Exception captured: %s %n", e.getMessage().toString());
        }

        for(int i = 0 ; i< ROWS; i++ ) {
            for(int j = 0 ; j< COLS; j++ )
                try {
                    System.out.print( matrixSA.get( i, j )+" " );
                } catch (MatrixIndexOutOfBoundsException e) {
                    System.out.printf("Exception captured: %s %n", e.getMessage().toString());
                }
            System.out.println();
        }
    }
}
