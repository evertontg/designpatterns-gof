package main.edu.drexel.patterns.structural.bridge;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/** Abstraction */
public abstract class Matrix {

    int rows, cols;
    List data ;

    protected Matrix( int rows, int cols, SArrayList list ) {
        this.rows = rows;
        this.cols = cols;
        data = list;
    }

    protected MatrixCell add(int row, int col ) throws MatrixIndexOutOfBoundsException {
        MatrixCell mc = getPosition( row, col );
        if( mc == null )
            mc = new MatrixCell( row, col );
        data.add( mc );
        return mc;
    }

    protected void remove(MatrixCell cell ) throws MatrixIndexOutOfBoundsException {
        data.remove( cell );
    }

    protected MatrixCell getPosition( int row, int col )
            throws MatrixIndexOutOfBoundsException {
        if( row < 0 || row >= this.rows || col < 0 || col >= this.cols )
            throw new MatrixIndexOutOfBoundsException();
        Iterator it = data.iterator();
        while( it.hasNext() ) {
            MatrixCell mc = (MatrixCell) it.next();
            if( mc.row == row && mc.col == col )
                return mc;
        }
        return null;
    }

    public abstract void put( int row, int col, int value ) throws MatrixIndexOutOfBoundsException;

    public abstract int get( int row, int col ) throws MatrixIndexOutOfBoundsException;
}
