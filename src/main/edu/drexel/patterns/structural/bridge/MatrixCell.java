package main.edu.drexel.patterns.structural.bridge;

/** Model (it does not represent a role in the pattern */
public class MatrixCell {

    public int row, col, value;

    public MatrixCell( int row, int column ) {
        this.row = row;
        col = column;
        value = 0;
    }
}
