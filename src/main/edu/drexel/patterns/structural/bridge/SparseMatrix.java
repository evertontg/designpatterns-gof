package main.edu.drexel.patterns.structural.bridge;

import java.util.ArrayList;

/** Refined Abstraction */
public class SparseMatrix extends Matrix{

    public SparseMatrix( int rows, int cols, SArrayList list ) {
        super( rows, cols, list );
    }

    @Override
    public void put( int row, int col, int value ) throws MatrixIndexOutOfBoundsException {
        MatrixCell cell = getPosition( row, col );

        if( cell != null )
            if( value == 0 ) {
                remove( cell );
            } else {
                cell.value = value;
            }
        else{
            if( value != 0 ) {
                cell = add( row, col );
                cell.value = value;
            }
        }
    }

    @Override
    public int get( int row, int col ) throws MatrixIndexOutOfBoundsException {
        MatrixCell cell = getPosition( row, col );

        if( cell == null) {
            return 0;
        }else {
            return cell.value;
        }
    }
}
