package main.edu.drexel.patterns.structural.bridge;

import java.util.List;

/** Implementor (abstract) */
public interface SList extends List{
}
