package main.edu.drexel.patterns.structural.bridge;

import java.util.ArrayList;

/** Refined Abstraction */
public class CompleteMatrix extends Matrix {

    public CompleteMatrix( int rows, int cols, SArrayList list ) {
        super( rows, cols, list );

        for(int i = 0 ; i< rows; i++ ) {
            for (int j = 0; j < cols; j++) {
                try { /** Exception Handling */
                    add(i, j);
                } catch (MatrixIndexOutOfBoundsException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void put( int row, int col, int value ) throws MatrixIndexOutOfBoundsException{
        MatrixCell cell = getPosition( row, col );
        cell.value = value;
    }

    @Override
    public int get( int row, int col ) throws MatrixIndexOutOfBoundsException {
        MatrixCell cell = getPosition( row, col );
        return cell.value;
    }
}
