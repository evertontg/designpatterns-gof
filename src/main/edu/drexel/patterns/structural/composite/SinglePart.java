package main.edu.drexel.patterns.structural.composite;

/** Leaf */
public class SinglePart extends Component{

    public SinglePart(String name) {
        super(name);
    }

    @Override
    public void describe(){
        System.out.println( "Component: " + name );
    }
}
