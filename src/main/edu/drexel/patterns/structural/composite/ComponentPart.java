package main.edu.drexel.patterns.structural.composite;

import java.util.ArrayList;

/** Composite */
public class ComponentPart extends Component{

    private ArrayList children ;

    public ComponentPart(String name) {
        super(name);
        children = new ArrayList();
    }

    public void describe(){
        System.out.println("Component: " + name+"is composed by");

        for( int i=0; i< children.size(); i ++ ) {
            Component component = (Component) children.get( i );
            component.describe();
        }
    }

    public void add(Component component) throws SinglePartException {
        children.add(component);
    }

    public void remove(Component component) throws SinglePartException{
        children.remove(component);
    }
    public Component getChild(int index) {
        return (Component) children.get(index);
    }
}
