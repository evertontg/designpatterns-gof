package main.edu.drexel.patterns.structural.composite;

public class SinglePartException extends Exception {
    public SinglePartException( ){
        super( "Not supported method" );
    }
}
