package main.edu.drexel.patterns.structural.decorator;

/** Component */
public interface Employee {

    String getName();
    String getOffice();
    void getDescription();

}
