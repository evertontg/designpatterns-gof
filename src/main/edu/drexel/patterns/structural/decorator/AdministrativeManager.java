package main.edu.drexel.patterns.structural.decorator;

/** Concrete Decorator */
public class AdministrativeManager extends ResponsibleWorker{

    public AdministrativeManager(Employee employee) {
        super(employee);
    }

    private void getMessage(){
        System.out.print( "I am a boss. " );
    }

    @Override
    public void getDescription() {
        getMessage();
        super.getDescription();
    }


}
