package main.edu.drexel.patterns.structural.decorator;

/** Concrete Decorator */
public class ProjectManager extends ResponsibleWorker{

    private String project;

    public ProjectManager( Employee empl, String proj ) {
        super( empl );
        project = proj;
    }
    public void getDescription() {
        super.getDescription();
        System.out.println( "I am the Manager of the Project:" + project );
    }
}
