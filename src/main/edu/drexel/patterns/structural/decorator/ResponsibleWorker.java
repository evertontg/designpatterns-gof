package main.edu.drexel.patterns.structural.decorator;

/** Decorator */
public abstract class ResponsibleWorker implements Employee{

    protected Employee responsibleWorker;

    public ResponsibleWorker(Employee employee) {
        responsibleWorker = employee;
    }

    @Override
    public String getName() {
        return responsibleWorker.getName();
    }

    @Override
    public String getOffice() {
        return responsibleWorker.getOffice();
    }

    @Override
    public void getDescription() {
        responsibleWorker.getDescription();
    }
}
