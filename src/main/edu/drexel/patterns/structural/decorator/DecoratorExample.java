package main.edu.drexel.patterns.structural.decorator;

public class DecoratorExample {

    public static void main(String arg[]) {
        Employee employee = new Engineer( "Everton Guimaraes", "Department of Computer Science" );
        System.out.println( "Who are you?");
        employee.getDescription();

        employee = new AdministrativeManager( employee );
        System.out.println( "Who are you now?");
        employee.getDescription();

        employee = new ProjectManager( employee,"Pattern Detection" );
        System.out.println( "Who are you now?");
        employee.getDescription();

        employee = new ProjectManager( employee, "Code Agglomerations" );
        System.out.println( "Who are you now?");
        employee.getDescription();
    }
}
