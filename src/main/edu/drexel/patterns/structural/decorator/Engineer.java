package main.edu.drexel.patterns.structural.decorator;

/** Concrete Component */
public class Engineer implements Employee{

    private String name, office;

    public Engineer(String name, String office) {
        this.name = name;
        this.office = office;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getOffice() {
        return office;
    }

    @Override
    public void getDescription() {
        System.out.println( "I am " + getName() + ", and I am with the "
                + getOffice() +".");
    }
}
