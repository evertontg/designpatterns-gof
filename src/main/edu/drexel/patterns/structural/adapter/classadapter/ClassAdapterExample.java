package main.edu.drexel.patterns.structural.adapter.classadapter;

import main.edu.drexel.patterns.structural.adapter.Polygon;

public class ClassAdapterExample {

    public static void main(String[] args) {

        Polygon polygon = new RectangleClassAdapter();
        polygon.setId( "Demo" );
        polygon.define( 3 , 4 , 10, 20, "RED" );

        /** Output */
        System.out.println( "The area of "+ polygon.getId() + " is "+
                polygon.getSurface() + ", and it’s " +
                polygon.getColor() );
    }
}
