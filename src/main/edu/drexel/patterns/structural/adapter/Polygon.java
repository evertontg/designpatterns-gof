package main.edu.drexel.patterns.structural.adapter;

/** Target Interface */
public interface Polygon {

    void define( float x0, float y0, float x1, float y1, String color );
    float[] getCoordinates() ;
    float getSurface();
    void setId( String id );
    String getId( );
    String getColor();
}
